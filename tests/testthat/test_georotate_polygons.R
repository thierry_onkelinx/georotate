context("georotate a Polygons object")
object <- sp::Polygon(
  coords = cbind(
    X = c(0.25, 0.25, 0.25, 0.25),
    Y = c(0.25, 0.75, 0.75, 0.25)
  )
)
object <- sp::Polygons(list(object), ID = 1)
controlpoints <-  rbind(
  c(0, 0, 1001, 1001),
  c(1, 0, 1000, 1001),
  c(0, 1, 1001, 1000),
  c(1, 1, 1000.1, 1000.1)
)

correct.output <- cbind(
  X = rep(1000.7625, 4),
  Y = c(1000.7625, 1000.2875, 1000.2875, 1000.7625)
)
attr(correct.output, "rmse") <- 0.05
attr(correct.output, "scale") <- 0.95
attr(correct.output, "radian") <- pi / 2
attr(correct.output, "degree") <- 90
attr(correct.output, "translation") <- c(1000.525, 1000.525)
attr(correct.output, "rotationcenter") <- c(0.5, 0.5)
correct.output <- sp::Polygon(correct.output)
correct.output <- sp::Polygons(list(correct.output), ID = 1)

expect_equal(
  georotate(
    object = object,
    controlpoints = controlpoints
  ),
  correct.output
)
